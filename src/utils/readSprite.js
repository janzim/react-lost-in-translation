import React from 'react';

class Sprite {
	constructor(xPos, yPos) {
		this.xPos = xPos;
		this.yPos = yPos;
	}
}

const spriteSize = 150;
const columns = 4;
const rows = 8;
const spriteCount = 27; // 27 because 1 extra sprite that acts as a "space"
const asciiStart = 97;
const asciiEnd = 122;

const getSpritesList = () => {
	const spriteCoordinates = [];

	// Divide the spritemap into coordinates
	for (let y = 0; y < columns; y++) {
		for (let x = 0; x < rows; x++) {
			if (y * rows + x >= spriteCount) {
				break;
			}
			const newSprite = new Sprite(
				x * spriteSize * -1,
				y * spriteSize * -1,
				y * rows + x
			);
			spriteCoordinates.push(newSprite);
		}
	}

	// Map coordinates to according sprites and put in a <div>
	const sprites = spriteCoordinates.map((s) => {
		return (
			<div
				style={{
					width: '150px',
					height: '150px',
					display: 'inline-block',
					background: `url(/sign-spritesheet-1200.png) ${s.xPos}px ${s.yPos}px`,
				}}
			></div>
		);
	});

	return sprites;
};

const getSprites = (text, setInvalidCharacters) => {
	const sprites = getSpritesList();
	const elSprites = [];

	[...text].forEach((c, index) => {
		if (
			c !== ' ' &&
			(c.charCodeAt() < asciiStart || c.charCodeAt() > asciiEnd)
		) {
			setInvalidCharacters(true);
		}
		if (c === ' ') {
			elSprites.push(<span key={index}>{sprites[26]}</span>);
		} else {
			elSprites.push(
				<span key={index}>{sprites[c.charCodeAt() - asciiStart]}</span>
			);
		}
	});

	return elSprites;
};

export default getSprites;
