import React, { useState } from 'react';
import InputBox from './InputBox';
import getSprites from '../utils/readSprite';
import { getUsername, addHistory } from '../utils/storage';
import { Redirect } from 'react-router-dom';

function TranslatorPage(props) {
	const [spritesToShow, setSpritesToShow] = useState([]);
	const [inputText, setInputText] = useState('');
	const [invalidCharacters, setInvalidCharacters] = useState(false);
	const isLoggedIn = getUsername();

	/**
	 * Convert text from this state into the specific sprites
	 */
	const displaySprites = () => {
		setInvalidCharacters(false);
		const elSprites = getSprites(inputText, setInvalidCharacters);
		setSpritesToShow(elSprites);

		// set storage
		addHistory(inputText);
	};

	return (
		<div>
			{!isLoggedIn && <Redirect to="/login" />}
			<div className="container">
				<div className="row justify-content-md-center">
					<div className="col col-lg-5">
						<InputBox
							invalidCharacters={invalidCharacters}
							displaySprites={displaySprites}
							setInputText={setInputText}
						></InputBox>
					</div>
				</div>
				<br></br>
				<div className="row justify-content-md-center border border-primary rounded">
					<div className="col">
						<div className="d-flex flex-wrap mh-100">
							{spritesToShow}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default TranslatorPage;
