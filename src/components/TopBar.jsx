import React from 'react';
import ProfileButton from './ProfileButton';

function TopBar() {
	return (
		<nav className="navbar navbar-dark bg-dark justify-content-between">
			<a className="navbar-brand mb-0 h1" href="/translate">
				Lost in Translation
			</a>
			<ProfileButton></ProfileButton>
		</nav>
	);
}

export default TopBar;
