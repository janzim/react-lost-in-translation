import React from 'react';

function ErrorPage() {
	return (
		<div>
			<img
				className="m-3"
				src="/logo-sad.png"
				style={{ width: '300px' }}
			></img>
			<h1>404 page not found</h1>
		</div>
	);
}

export default ErrorPage;
