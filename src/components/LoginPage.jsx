import FormControl, { input } from 'react-bootstrap/FormControl';
import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { setUsername, getUsername } from '../utils/storage';
import { Redirect } from 'react-router-dom';

function LoginPage() {
	const [username, setUser] = useState('');
	const isLoggedIn = getUsername('user');

	const onUsernameChange = (e) => {
		setUser(e.target.value.trim());
	};

	const onLoginClick = () => {
		login(username);
	};

	const onKeyPress = (e) => {
		if (e.key === 'Enter') login(username);
	};

	return (
		<div>
			{isLoggedIn && <Redirect to="/translate" />}
			<div className="container w-25">
				<div className="row">
					<div className="col-md text-center p-3">
						<Form>
							<Form.Group>
								<Form.Control
									type="text"
									placeholder="Username"
									onChange={onUsernameChange}
									onKeyPress={onKeyPress}
									maxLength="16"
								/>
							</Form.Group>
							<Button
								variant="primary"
								type="submit"
								onClick={onLoginClick}
							>
								Login
							</Button>
						</Form>
					</div>
				</div>
			</div>
			<img
				className="border rounded"
				src="/dancingrobot.gif"
				style={{ width: '500px' }}
			/>
		</div>
	);
}

const login = (username) => {
	try {
		setUsername(username);
	} catch (e) {
		console.error(e);
	}
};

export default LoginPage;
