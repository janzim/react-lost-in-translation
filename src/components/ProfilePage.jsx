import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { getUsername, getHistory, clearHistory } from '../utils/storage';
import { Row, Button } from 'react-bootstrap';

// update the state to force render
function useForceUpdate() {
	const [value, setValue] = useState(false);
	return () => setValue((value) => !value);
}

function ProfilePage() {
	const isLoggedIn = getUsername();
	const history = getHistory();
	const forceUpdate = useForceUpdate();

	const onClearHistoryClick = () => {
		clearHistory();
		forceUpdate();
	};

	return (
		<div>
			<div className="container">
				{!isLoggedIn && <Redirect to="/login" />}
				<div className="row justify-content-center align-items-center">
					<h1 className="col-2 pb-3">History</h1>
				</div>
			</div>
			<div className="container">
				<div className="row justify-content-center">
					<div className="col-5">
						<div className="container">
							{history.reverse().map((item, i) => {
								return (
									<div className="row">
										<div className="col m-1">
											<span
												class="badge badge-secondary"
												style={{ fontSize: '18px' }}
											>
												{i + 1}.
											</span>
										</div>
										<div className="col-sm-10">
											<span
												className="text-break"
												key={i}
												style={{
													fontSize: '18px',
													overflowWrap: 'break-word', // chrome
													wordBreak: 'break-word', // edge
												}}
											>
												{item}
											</span>
										</div>
									</div>
								);
							})}
						</div>
					</div>
					<div className="col-2">
						<Button variant="danger" onClick={onClearHistoryClick}>
							Clear History
						</Button>
					</div>
				</div>
			</div>
		</div>
	);
}

export default ProfilePage;
