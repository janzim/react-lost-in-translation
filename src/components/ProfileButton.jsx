import React, { useEffect, useState } from 'react';
import { Dropdown } from 'bootstrap/dist/js/bootstrap.bundle';
import { getUsername, clearUsername, clearHistory } from '../utils/storage';
import { Redirect } from 'react-router-dom';

// update the state to force render
function useForceUpdate() {
	const [value, setValue] = useState(false);
	return () => setValue((value) => !value);
}

function ProfileButton() {
	const isLoggedIn = getUsername();
	const forceUpdate = useForceUpdate();

	const onLogoutClick = () => {
		clearUsername();
		clearHistory();
		forceUpdate();
	};

	return (
		<div>
			{isLoggedIn && (
				<div class="btn-group">
					<button
						type="button"
						class="btn btn-secondary dropdown-toggle"
						data-toggle="dropdown"
						aria-haspopup="true"
						aria-expanded="false"
					>
						<img src="Logo-Hello.png" alt="profile" width="40px" />
					</button>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="/profile">
							Logged in as{' '}
							<span class="font-weight-bold">{isLoggedIn}</span>
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="/profile">
							Profile
						</a>
						<a
							class="dropdown-item"
							href="/login"
							onClick={onLogoutClick}
						>
							Logout
						</a>
					</div>
				</div>
			)}
		</div>
	);
}

export default ProfileButton;
