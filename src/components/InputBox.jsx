import React from 'react';

function InputBox(props) {
	const invalidCharacters = props.invalidCharacters;
	const displaySprites = props.displaySprites;
	const setInputText = props.setInputText;

	const onInputChange = (e) => {
		setInputText(e.target.value.toLowerCase().trim());
	};

	const onKeyPress = (e) => {
		if (e.key === 'Enter') {
			displaySprites();
		}
	};

	return (
		<div>
			<div className="pt-3 pl-3 input-group">
				<input
					type="text"
					className="form-control"
					placeholder="Enter text"
					onChange={onInputChange}
					onKeyPress={onKeyPress}
					maxLength="40"
				/>
				<div className="input-group-append">
					<button
						className="btn btn-primary"
						type="button"
						onClick={displaySprites}
					>
						Display
					</button>
				</div>
			</div>
			{invalidCharacters && (
				<div className="input-group-append">
					<label className="text-warning pl-3 pt-1 font-italic">
						Text contains non-english characters, symbols or
						numbers. These will be ignored
					</label>
				</div>
			)}
		</div>
	);
}

export default InputBox;
