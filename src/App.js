import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import TranslatorPage from './components/TranslatorPage';
import LoginPage from './components/LoginPage';
import TopBar from './components/TopBar';
import ProfilePage from './components/ProfilePage';
import ErrorPage from './components/ErrorPage';
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Redirect,
} from 'react-router-dom';

function App() {
	return (
		<Router>
			<div className="App">
				<TopBar></TopBar>
				<Switch>
					<Route exact path="/">
						<Redirect to="/login" />
					</Route>
					<Route path="/login" component={LoginPage} />
					<Route path="/translate" component={TranslatorPage} />
					<Route path="/profile" component={ProfilePage} />
					<Route path="*" component={ErrorPage} />
				</Switch>
			</div>
		</Router>
	);
}

export default App;
