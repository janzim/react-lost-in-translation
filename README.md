# React "Lost in Translation" (Experis Assignment)

The task for this assignment was to create a website in React that is supposed to "translate" regular text into sign language.

## Features

The user should be able to log in with a username. Once logged in, the user can then write in the desired text into a text box and once submitted, the sign language should then be generated as a set of images below.

The user has also the ability to see their last 10 translated texts and the ability to clear this history.

If the user decides to logout through the profile dropdown menu, all storage (including history) will be cleared. If the user enters an invalid path (e.g. "/asd"), he will be redirected to a 404 page.

See the showcase below for the outlook of the application.

## Showcase

### Login screen (/login)

![](showcase/1.PNG)

---

### Translator page (/translate)

![](showcase/2.PNG)

---

### Trying to input invalid characters

![](showcase/3.PNG)

---

### Profile dropdown menu

![](showcase/4.PNG)

---

### Profile page (/profile)

![](showcase/5.PNG)

---

### 404 page on invalid URL (/\*)

![](showcase/6.PNG)

## Author

Jan Zimmer
